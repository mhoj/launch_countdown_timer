import './App.scss';

import dayjs from 'dayjs';
import Countdown from 'react-countdown';
import FlipClock from './components/FlipClock';
import Header from './components/Header';
import Footer from './components/Footer';

function App() {
  const countFromDate = dayjs()
    .add(8, 'day')
    .add(23, 'hour')
    .add(55, 'minute')
    .add(41, 'second')
    .toDate();

  // Renderer callback with condition
  const renderer = ({days, hours, minutes, seconds, completed}) => {
    return (
      <div className="countdown-container">
          <Header text={completed ? "You are good to go!" : "We are launcing soon"}/>
          <FlipClock days={days} hours={hours} minutes={minutes} seconds={seconds} />
          <Footer />
      </div>
    );
  };

  return (
    <div className="App">
        <Countdown date={countFromDate} renderer={renderer}/>
    </div>
  );
}

export default App;