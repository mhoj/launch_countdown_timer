import IconFb from './icons/IconFb';
import IconIns from './icons/IconIns';
import IconPin from './icons/IconPin';


function FooterIcon({link, name, icon}) {
    return (
        <a href={link} className="footer__icon">
            <span className="sr-only">{name}</span>
            {icon}
        </a>
    );
}

export default function Footer() {
    return (
        <footer className="footer">
            <FooterIcon
                link="https://www.facebook.com/"
                name="facebook link"
                icon= { <IconFb/>}
            />
            <FooterIcon
                link="https://www.pinterest.com/"
                name="pinterest link"
                icon= { <IconIns/>}
            />
            <FooterIcon
                link="https://www.instagram.com/"
                name="instagram link"
                icon= { <IconPin/>}
            />
        </footer>
    );
}