export default function Header({text}) {
    return (
        <header className="header">
            <h1 className="t-header">{text}</h1>
        </header>
    )
}