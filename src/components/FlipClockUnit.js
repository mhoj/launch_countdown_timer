import StaticCard from './StaticCard';

export default function FlipClockUnit({ number, unit }) {
    const currNumber = number;
    return (
        <div className="flip-clock-unit">
            <div className="flip-clock-card">
                <div className="flip-clock-card__hole flip-clock-card__hole--left"></div>
                <div className="flip-clock-card__hole flip-clock-card__hole--right"></div>
                <div className="flip-clock-card__bar"></div>
                <StaticCard position="top" number={currNumber} />
                <StaticCard position="bottom" number={currNumber} />
            </div>
            <p className="flip-clop-unit__caption t-unit">{unit}</p>
        </div>
    )
}